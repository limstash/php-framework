<?php
    /**
     * Framework Module: ip-database
     * Get IP Address Location
     * 
     * IPv4 by cz88.net: https://blog.csdn.net/myweishanli/article/details/45098693
     * IPv6 by ZX Inc: https://github.com/ZX-Inc/zxipdb-php
     * 
     * @package framework
     * @since 2.0
     */


    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class FrameworkIPv4Location
    {
        /**
         * QQWry.Dat文件指针
         *
         * @var resource
         */
        private $fp;
        /**
         * 第一条IP记录的偏移地址
         *
         * @var int
         */
        private $firstip;
        /**
         * 最后一条IP记录的偏移地址
         *
         * @var int
         */
        private $lastip;
        /**
         * IP记录的总条数（不包含版本信息记录）
         *
         * @var int
         */
        private $totalip;
    
        /**
         * 构造函数，打开 QQWry.Dat 文件并初始化类中的信息
         *
         * @param string $filename
         */
        public function __construct($filename)
        {
            $this->fp = 0;
            if (($this->fp = fopen($filename, 'rb')) !== false) {
                $this->firstip = $this->getlong();
                $this->lastip = $this->getlong();
                $this->totalip = ($this->lastip - $this->firstip) / 7;
                //注册析构函数，使其在程序执行结束时执行
                register_shutdown_function(array(
                    &$this,
                    '__destruct'
                ));
            }
        }
    
        /**
         * 析构函数，用于在页面执行结束后自动关闭打开的文件。
         *
         */
        public function __destruct()
        {
            if ($this->fp) {
                fclose($this->fp);
            }
            $this->fp = 0;
        }
    
        /**
         * 返回读取的长整型数
         *
         * @access private
         * @return int
         */
        private function getlong()
        {
            //将读取的little-endian编码的4个字节转化为长整型数
            $result = unpack('Vlong', fread($this->fp, 4));
            return $result['long'];
        }
    
        /**
         * 返回读取的3个字节的长整型数
         *
         * @access private
         * @return int
         */
        private function getlong3()
        {
            //将读取的little-endian编码的3个字节转化为长整型数
            $result = unpack('Vlong', fread($this->fp, 3) . chr(0));
            return $result['long'];
        }
    
        /**
         * 返回压缩后可进行比较的IP地址
         *
         * @access private
         * @param string $ip
         * @return string
         */
        private function packip($ip)
        {
            // 将IP地址转化为长整型数，如果在PHP5中，IP地址错误，则返回False，
            // 这时intval将Flase转化为整数-1，之后压缩成big-endian编码的字符串
            return pack('N', intval(ip2long($ip)));
        }
    
        /**
         * 返回读取的字符串
         *
         * @access private
         * @param string $data
         * @return string
         */
        private function getstring($data = "")
        {
            $char = fread($this->fp, 1);
            while (ord($char) > 0) { // 字符串按照C格式保存，以结束
                $data .= $char; // 将读取的字符连接到给定字符串之后
                $char = fread($this->fp, 1);
            }
            return iconv('gbk', 'utf-8', $data);
        }
    
        /**
         * 返回地区信息
         *
         * @access private
         * @return string
         */
        private function getarea()
        {
            $byte = fread($this->fp, 1); // 标志字节
            switch (ord($byte)) {
                case 0: // 没有区域信息
                    $area = "";
                    break;
                case 1:
                case 2: // 标志字节为1或2，表示区域信息被重定向
                    fseek($this->fp, $this->getlong3());
                    $area = $this->getstring();
                    break;
                default: // 否则，表示区域信息没有被重定向
                    $area = $this->getstring($byte);
                    break;
            }
            return $area;
        }
    
        /**
         * 根据所给 IP 地址或域名返回所在地区信息
         *
         * @access public
         * @param string $ip
         * @return array
         */
        public function getlocation($ip)
        {
            if (!$this->fp)
                return null; // 如果数据文件没有被正确打开，则直接返回空
            $location['ip'] = gethostbyname($ip); // 将输入的域名转化为IP地址
            $ip = $this->packip($location['ip']); // 将输入的IP地址转化为可比较的IP地址
            // 不合法的IP地址会被转化为255.255.255.255
            // 对分搜索
            $l = 0; // 搜索的下边界
            $u = $this->totalip; // 搜索的上边界
            $findip = $this->lastip; // 如果没有找到就返回最后一条IP记录（QQWry.Dat的版本信息）
            while ($l <= $u) { // 当上边界小于下边界时，查找失败
                $i = floor(($l + $u) / 2); // 计算近似中间记录
                fseek($this->fp, $this->firstip + $i * 7);
                $beginip = strrev(fread($this->fp, 4)); // 获取中间记录的开始IP地址
                // strrev函数在这里的作用是将little-endian的压缩IP地址转化为big-endian的格式
                // 以便用于比较，后面相同。
                if ($ip < $beginip) { // 用户的IP小于中间记录的开始IP地址时
                    $u = $i - 1; // 将搜索的上边界修改为中间记录减一
                } else {
                    fseek($this->fp, $this->getlong3());
                    $endip = strrev(fread($this->fp, 4)); // 获取中间记录的结束IP地址
                    if ($ip > $endip) { // 用户的IP大于中间记录的结束IP地址时
                        $l = $i + 1; // 将搜索的下边界修改为中间记录加一
                    } else { // 用户的IP在中间记录的IP范围内时
                        $findip = $this->firstip + $i * 7;
                        break; // 则表示找到结果，退出循环
                    }
                }
            }
            //获取查找到的IP地理位置信息
            fseek($this->fp, $findip);
            $location['beginip'] = long2ip($this->getlong()); // 用户IP所在范围的开始地址
            $offset = $this->getlong3();
            fseek($this->fp, $offset);
            $location['endip'] = long2ip($this->getlong()); // 用户IP所在范围的结束地址
            $byte = fread($this->fp, 1); // 标志字节
            switch (ord($byte)) {
                case 1: // 标志字节为1，表示国家和区域信息都被同时重定向
                    $countryOffset = $this->getlong3(); // 重定向地址
                    fseek($this->fp, $countryOffset);
                    $byte = fread($this->fp, 1); // 标志字节
                    switch (ord($byte)) {
                        case 2: // 标志字节为2，表示国家信息又被重定向
                            fseek($this->fp, $this->getlong3());
                            $location['country'] = $this->getstring();
                            fseek($this->fp, $countryOffset + 4);
                            $location['area'] = $this->getarea();
                            break;
                        default: // 否则，表示国家信息没有被重定向
                            $location['country'] = $this->getstring($byte);
                            $location['area'] = $this->getarea();
                            break;
                    }
                    break;
                case 2: // 标志字节为2，表示国家信息被重定向
                    fseek($this->fp, $this->getlong3());
                    $location['country'] = $this->getstring();
                    fseek($this->fp, $offset + 8);
                    $location['area'] = $this->getarea();
                    break;
                default: // 否则，表示国家信息没有被重定向
                    $location['country'] = $this->getstring($byte);
                    $location['area'] = $this->getarea();
                    break;
            }
            if ($location['country'] == " CZ88.NET") { // CZ88.NET表示没有有效信息
                $location['country'] = "未知";
            }
            if ($location['area'] == " CZ88.NET") {
                $location['area'] = "";
            }
            return $location;
        }
    }

    class FrameworkIPv6Location
    {
        const FORMAT = 'J2';
        public static $file;
        private static $total = null;
        // 索引区
        private static $index_start_offset;
        private static $index_end_offset;
        private static $offlen;
        private static $iplen;
        private static $has_initialized = false;

        /**
         * return database record count
         * @return int|string
         */
        public static function total()
        {
            if (null === static::$total) {
                $fd = fopen(static::$file, 'rb');
                static::initialize($fd);
                fclose($fd);
            }
            return static::$total;
        }

        /**
         * judge IP address is valid
         * @param $ip
         * @return bool
         */
        public static function isValidAddress($ip)
        {
            return $ip === filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
        }

        public static function initialize($fd)
        {
            if (!static::$has_initialized) {
                if (PHP_INT_SIZE < 8) {
                    throw new RuntimeException('64bit OS supported only');
                }
                if (version_compare(PHP_VERSION, "5.6", "<")) {
                    throw new RuntimeException('php version 5.6 or greater');
                }
                static::$index_start_offset = static::read8($fd, 16);
                static::$offlen = static::read1($fd, 6);
                static::$iplen = static::read1($fd, 7);
                static::$total = static::read8($fd, 8);
                static::$index_end_offset = static::$index_start_offset
                    + (static::$iplen + static::$offlen) * static::$total;
                static::$has_initialized = true;
            }
        }

        /**
         * query ipv6
         * @param $ip
         * @return array
         */
        public static function query($ip)
        {
            if (!self::isValidAddress($ip)) {
                throw new RuntimeException("error IPv6 address: $ip");
            }
            $ip_bin = inet_pton($ip);
            $fd = fopen(static::$file, 'rb');
            static::initialize($fd);
            $ip_num_arr = unpack(static::FORMAT, $ip_bin);
            // IP地址前半部分转换成有int
            $ip_num1 = $ip_num_arr[1];
            // IP地址后半部分转换成有int
            $ip_num2 = $ip_num_arr[2];
            $ip_find = static::find($fd, $ip_num1, $ip_num2, 0, static::$total);
            $ip_offset = static::$index_start_offset + $ip_find * (static::$iplen + static::$offlen);
            $ip_offset2 = $ip_offset + static::$iplen + static::$offlen;
            $ip_start = inet_ntop(pack(static::FORMAT, static::read8($fd, $ip_offset), 0));
            try {
                $ip_end = inet_ntop(pack(static::FORMAT, static::read8($fd, $ip_offset2) - 1, 0));
            } catch (RuntimeException $e) {
                $ip_end = "FFFF:FFFF:FFFF:FFFF::";
            }
            $ip_record_offset = static::read8($fd, $ip_offset + static::$iplen, static::$offlen);
            $ip_addr = static::read_record($fd, $ip_record_offset);
            $ip_addr_disp = $ip_addr[0] . " " . $ip_addr[1];
            if (is_resource($fd)) {
                fclose($fd);
            }
            return array("start" => $ip_start, "end" => $ip_end, "addr" => $ip_addr, "disp" => $ip_addr_disp);
        }

        /**
         * 读取记录
         * @param $fd
         * @param $offset
         * @return string[]
         */
        public static function read_record($fd, $offset)
        {
            $record = array(0 => "", 1 => "");
            $flag = static::read1($fd, $offset);
            if ($flag == 1) {
                $location_offset = static::read8($fd, $offset + 1, static::$offlen);
                return static::read_record($fd, $location_offset);
            }
            $record[0] = static::read_location($fd, $offset);
            if ($flag == 2) {
                $record[1] = static::read_location($fd, $offset + static::$offlen + 1);
            } else {
                $record[1] = static::read_location($fd, $offset + strlen($record[0]) + 1);
            }
            return $record;
        }

        /**
         * 读取地区
         * @param $fd
         * @param $offset
         * @return string
         */
        public static function read_location($fd, $offset)
        {
            if ($offset == 0) {
                return "";
            }
            $flag = static::read1($fd, $offset);
            // 出错
            if ($flag == 0) {
                return "";
            }
            // 仍然为重定向
            if ($flag == 2) {
                $offset = static::read8($fd, $offset + 1, static::$offlen);
                return static::read_location($fd, $offset);
            }
            return static::readstr($fd, $offset);
        }

        /**
         * 查找 ip 所在的索引
         * @param $fd
         * @param $ip_num1
         * @param $ip_num2
         * @param $l
         * @param $r
         * @return mixed
         */
        public static function find($fd, $ip_num1, $ip_num2, $l, $r)
        {
            if ($l + 1 >= $r) {
                return $l;
            }
            $m = intval(($l + $r) / 2);
            $m_ip1 = static::read8($fd, static::$index_start_offset + $m * (static::$iplen + static::$offlen), static::$iplen);
            $m_ip2 = 0;
            if (static::$iplen <= 8) {
                $m_ip1 <<= 8 * (8 - static::$iplen);
            } else {
                $m_ip2 = static::read8($fd, static::$index_start_offset + $m * (static::$iplen + static::$offlen) + 8, static::$iplen - 8);
                $m_ip2 <<= 8 * (16 - static::$iplen);
            }
            if (static::uint64cmp($ip_num1, $m_ip1) < 0) {
                return static::find($fd, $ip_num1, $ip_num2, $l, $m);
            }
            if (static::uint64cmp($ip_num1, $m_ip1) > 0) {
                return static::find($fd, $ip_num1, $ip_num2, $m, $r);
            }
            if (static::uint64cmp($ip_num2, $m_ip2) < 0) {
                return static::find($fd, $ip_num1, $ip_num2, $l, $m);
            }
            return static::find($fd, $ip_num1, $ip_num2, $m, $r);
        }

        public static function readraw($fd, $offset = null, $size = 0)
        {
            if (!is_null($offset)) {
                fseek($fd, (int)$offset);
            }
            return fread($fd, $size);
        }

        public static function read1($fd, $offset = null)
        {
            if (!is_null($offset)) {
                fseek($fd, (int)$offset);
            }
            $a = fread($fd, 1);
            return @unpack("C", $a)[1];
        }

        public static function read8($fd, $offset = null, $size = 8)
        {
            if (!is_null($offset)) {
                fseek($fd, (int)$offset);
            }
            $a = fread($fd, $size) . "\0\0\0\0\0\0\0\0";
            return @unpack("P", $a)[1];
        }

        public static function readstr($fd, $offset = null)
        {
            if (!is_null($offset)) {
                fseek($fd, (int)$offset);
            }
            $str = "";
            $chr = static::read1($fd, $offset);
            while ($chr != 0) {
                $str .= chr($chr);
                $offset++;
                $chr = static::read1($fd, $offset);
            }
            return $str;
        }

        public static function ip2num($ip)
        {
            return unpack("N", inet_pton($ip))[1];
        }

        public static function inet_ntoa($nip)
        {
            $ip = array();
            for ($i = 3; $i > 0; $i--) {
                $ip_seg = intval($nip / pow(256, $i));
                $ip[] = $ip_seg;
                $nip -= $ip_seg * pow(256, $i);
            }
            $ip[] = $nip;
            return join(".", $ip);
        }

        public static function uint64cmp($a, $b)
        {
            if ($a >= 0 && $b >= 0 || $a < 0 && $b < 0) {
                if ($a === $b) {
                    return 0;
                }
                if ($a > $b) {
                    return 1;
                }
                return -1;
            }
            if ($a >= 0 && $b < 0) {
                return -1;
            }
            return 1;
        }
    }

    /**
     * Get Visitor IP Location
     * 
     * @param string $ip
     * IP Address
     * @return string $location
     */

    frame::__extend("getIPLocation", function ($ip){
        if (FrameworkIPv6Location::isValidAddress($ip)) {
            FrameworkIPv6Location::$file = dirname(__FILE__) . "/assets/ipv6.db";
            $location = FrameworkIPv6Location::query($ip)["addr"][0];

            if (!empty($location)) {
                return $location;
            }
        }

        $IPv4location = new FrameworkIPv4Location(dirname(__FILE__) . "/assets/ipv4.dat");
        return $IPv4location->getlocation($ip)['country'];  
    });
?>
<?php

    /**
     * Print error message
	 * 
     * @package framework
     * @since 2.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }
	
	header("HTTP/1.1 500 Internal Server Error");

	$debug = $option["debug"];
	$framework_version = $option["framework_version"];
	$system_version = $option["system_version"];
	$request_id = $option["request_id"];
	$site_name = $option["site_name"];
	$site_url = $option["site_url"];

	if ($format_json) {
		$stack_size = count($stack);

		for ($i = 0; $i < $stack_size; $i++) {
			$stack[$i]["file"] = frame::styleFilepath($stack[$i]["file"]);
		}

		exit(json_encode([
			"id" => $request_id,
			"code" => $code,
			"msg" => frame::styleFilepath($title),
			"debug" => $debug == true ? $stack : "Debug Info has been disabled.",
			"version" => [
				"framework" => $framework_version,
				"system" => $system_version
			],
			"site" => [
				"name" => $site_name,
				"url" => $site_url
			]
		], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
	}
?>

<iframe width="100%" height="100%" onload="resize(this)" frameborder="0" marginwidth="0" marginheight="0" srcdoc='
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<title>An Error Occured<?= !empty($sitename) ? " - {$siteName}" : ""?></title>
		<meta charset="utf-8" />
	    <style>
			/*!
			* Bootstrap v5.0.2 (https://getbootstrap.com/)
			* Copyright 2011-2021 The Bootstrap Authors
			* Copyright 2011-2021 Twitter, Inc.
			* Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
			*/
			:root {
				--bs-blue: #0d6efd;
				--bs-indigo: #6610f2;
				--bs-purple: #6f42c1;
				--bs-pink: #d63384;
				--bs-red: #dc3545;
				--bs-orange: #fd7e14;
				--bs-yellow: #ffc107;
				--bs-green: #198754;
				--bs-teal: #20c997;
				--bs-cyan: #0dcaf0;
				--bs-white: #fff;
				--bs-gray: #6c757d;
				--bs-gray-dark: #343a40;
				--bs-primary: #0d6efd;
				--bs-secondary: #6c757d;
				--bs-success: #198754;
				--bs-info: #0dcaf0;
				--bs-warning: #ffc107;
				--bs-danger: #dc3545;
				--bs-light: #f8f9fa;
				--bs-dark: #212529;
				--bs-font-sans-serif: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
				--bs-font-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
				--bs-gradient: linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));
			}

			*,
			*::before,
			*::after {
				box-sizing: border-box;
			}

			@media (prefers-reduced-motion: no-preference) {
				:root {
					scroll-behavior: smooth;
				}
			}

			body {
				margin: 0;
				font-family: var(--bs-font-sans-serif);
				font-size: 1rem;
				font-weight: 400;
				line-height: 1.5;
				color: #212529;
				background-color: #fff;
				-webkit-text-size-adjust: 100%;
				-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			}

			.container,
			.container-fluid,
			.container-xxl,
			.container-xl,
			.container-lg,
			.container-md,
			.container-sm {
				width: 100%;
				padding-right: var(--bs-gutter-x, 0.75rem);
				padding-left: var(--bs-gutter-x, 0.75rem);
				margin-right: auto;
				margin-left: auto;
			}

			@media (min-width: 576px) {
				.container-sm, .container {
					max-width: 540px;
				}
			}
			@media (min-width: 768px) {
				.container-md, .container-sm, .container {
					max-width: 720px;
				}
			}
			@media (min-width: 992px) {
				.container-lg, .container-md, .container-sm, .container {
					max-width: 960px;
				}
			}
			@media (min-width: 1200px) {
				.container-xl, .container-lg, .container-md, .container-sm, .container {
					max-width: 1140px;
				}
			}
			@media (min-width: 1400px) {
				.container-xxl, .container-xl, .container-lg, .container-md, .container-sm, .container {
					max-width: 1320px;
				}
			}

			h6, .h6, h5, .h5, h4, .h4, h3, .h3, h2, .h2, h1, .h1 {
				margin-top: 0;
				margin-bottom: 0.5rem;
				font-weight: 500;
				line-height: 1.2;
			}

			h1, .h1 {
				font-size: calc(1.375rem + 1.5vw);
			}
			@media (min-width: 1200px) {
				h1, .h1 {
					font-size: 2.5rem;
				}
			}

			h3, .h3 {
				font-size: calc(1.3rem + 0.6vw);
			}
			@media (min-width: 1200px) {
				h3, .h3 {
					font-size: 1.75rem;
				}
			}

			.card {
				position: relative;
				display: flex;
				flex-direction: column;
				min-width: 0;
				word-wrap: break-word;
				background-color: #fff;
				background-clip: border-box;
				border: 1px solid rgba(0, 0, 0, 0.125);
				border-radius: 0.25rem;
			}

			.card-body {
				flex: 1 1 auto;
				padding: 1rem 1rem;
			}

			a {
				color: #0d6efd;
				text-decoration: none;
			}
            
			a:hover {
				color: #0a58ca;
			}

			a:not([href]):not([class]), a:not([href]):not([class]):hover {
				color: inherit;
				text-decoration: none;
			}

			small, .small {
				font-size: 0.875em;
			}

			table {
				caption-side: bottom;
				border-collapse: collapse;
			}

			th {
				text-align: inherit;
				text-align: -webkit-match-parent;
			}

			thead,
			tbody,
			tfoot,
			tr,
			td,
			th {
				border-color: inherit;
				border-style: solid;
				border-width: 0;
			}

			.table {
				--bs-table-bg: transparent;
				--bs-table-accent-bg: transparent;
				--bs-table-striped-color: #212529;
				--bs-table-striped-bg: rgba(0, 0, 0, 0.05);
				--bs-table-active-color: #212529;
				--bs-table-active-bg: rgba(0, 0, 0, 0.1);
				--bs-table-hover-color: #212529;
				--bs-table-hover-bg: rgba(0, 0, 0, 0.075);
				width: 100%;
				margin-bottom: 1rem;
				color: #212529;
				vertical-align: top;
				border-color: #dee2e6;
			}
			.table > :not(caption) > * > * {
				padding: 0.5rem 0.5rem;
				background-color: var(--bs-table-bg);
				border-bottom-width: 1px;
			box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
			}
			.table > tbody {
				vertical-align: inherit;
			}
			.table > thead {
				vertical-align: bottom;
			}
			.table > :not(:last-child) > :last-child > * {
				border-bottom-color: currentColor;
			}

			.table-bordered > :not(caption) > * {
				border-width: 1px 0;
			}
			.table-bordered > :not(caption) > * > * {
				border-width: 0 1px;
			}

			.table-striped > tbody > tr:nth-of-type(odd) {
				--bs-table-accent-bg: var(--bs-table-striped-bg);
				color: var(--bs-table-striped-color);
			}

			.table-hover > tbody > tr:hover {
				--bs-table-accent-bg: var(--bs-table-hover-bg);
				color: var(--bs-table-hover-color);
			}

			.table-success {
				--bs-table-bg: #d1e7dd;
				--bs-table-striped-bg: #c7dbd2;
				--bs-table-striped-color: #000;
				--bs-table-active-bg: #bcd0c7;
				--bs-table-active-color: #000;
				--bs-table-hover-bg: #c1d6cc;
				--bs-table-hover-color: #000;
				color: #000;
				border-color: #bcd0c7;
			}

			.table-warning {
				--bs-table-bg: #fff3cd;
				--bs-table-striped-bg: #f2e7c3;
				--bs-table-striped-color: #000;
				--bs-table-active-bg: #e6dbb9;
				--bs-table-active-color: #000;
				--bs-table-hover-bg: #ece1be;
				--bs-table-hover-color: #000;
				color: #000;
				border-color: #e6dbb9;
			}

			.table-danger {
				--bs-table-bg: #f8d7da;
				--bs-table-striped-bg: #eccccf;
				--bs-table-striped-color: #000;
				--bs-table-active-bg: #dfc2c4;
				--bs-table-active-color: #000;
				--bs-table-hover-bg: #e5c7ca;
				--bs-table-hover-color: #000;
				color: #000;
				border-color: #dfc2c4;
			}

			.table-responsive {
				overflow-x: auto;
				-webkit-overflow-scrolling: touch;
			}

			.mb-2 {
				margin-bottom: 0.5rem !important;
			}
			.mb-3 {
				margin-bottom: 1rem !important;
			}
			.mb-4 {
				margin-bottom: 1.5rem !important;
			}

			.text-center {
				text-align: center !important;
			}
			</style>
			
			<style>
			html{
				font-size: 14px;
			}
			.header {
				padding-top: 20px;
			}
			.footer {
				text-align: center;
				margin-top: 20px;
				padding-top: 20px;
				color: gray;
				border-top: 1px solid #e5e5e5;
				line-height: 10px;
			}
		</style>

	</head>
	<body role="document">
		<div class="container theme-showcase" role="main">
			<div class="header mb-2">
				<font color="red"><h1>System Error <?= $code ?></h1></font>
			</div>
			
			<div class="card mb-3">
				<div class="card-body"><?= frame::styleFilepath($title) ?></div>
			</div>
			
			<div>
				<h3>Debug Info</h3>
			</div>

			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th class="text-center" style="width:6em;">ID</th>
							<th class="text-center" style="width:40%;">File</th>
							<th class="text-center" style="width:6em;">Line</th>
							<th class="text-center">Function</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if(!$debug){
								echo "<tr class=\"text-center\"><td colspan=\"4\">Debug Info has been disabled.</td></tr>";
							}else{
								$stackCount = count($stack);

								if ($code != -1) {
									$attemped = 2;
								} else {
									$attemped = 0;
								}

								for($transferID = 0; $transferID < $stackCount; ++$transferID){
									if($stack[$transferID]["function"] == "writeLog"){
										$attemped = $transferID + 1;
									}
								}
								
								//Print Transfer Stack
								for($transferID = $stackCount - 1; $transferID >= 0; --$transferID){
									if($transferID > $attemped)
										echo "<tr class=\"text-center table-success\">";
									else if($transferID == $attemped)
										echo "<tr class=\"text-center table-danger\">";
									else
										echo "<tr class=\"text-center table-warning\">";

									$ID = $stackCount - $transferID;
									echo "<td>#{$ID}</td>";
									
									$fileName = frame::styleFilepath($stack[$transferID]['file']);

									echo "<td>{$fileName}</td>";
									echo "<td>{$stack[$transferID]['line']}</td>";
									
									echo "<td>";
									if(!empty($stack[$transferID]['class'])){
										echo "{$stack[$transferID]['class']} >> ";
									}
                                    if(!empty($stack[$transferID]['function'])) {
                                        echo "{$stack[$transferID]['function']}()";
                                    }
                                    echo "</td>";
									echo "</tr>";
								}
							}
                        ?>					
					</tbody>
				</table>
			</div>
			<div class="footer">
				<small>Framework <?= $framework_version ?><?php if(!empty($system_version)) echo " / System {$system_version}"; ?><?php if(!empty($request_id)) echo " / Request {$request_id}"; ?><?php if(!empty($site_name)) echo " / <a href=\"{$site_url}\">{$site_name}</a>"; ?></small>
			</div>
	</body>
</html>
'></iframe>
<script>function resize(obj) {obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';}</script>
<?php

    /**
     * Handle Exception
     * 
     * @package framework
     * @since 2.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class FrameworkException {
        public static function framework_exception_print($code, $title, $stack, array $option = []) {
            if ($stack == null) {
                $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            }

            if (!array_key_exists("framework_version", $option)) {
                $option["framework_version"] = constant("framework_version");
            }

            if (defined("FRAMEWORK_MODULELOAD/config-parser")) {
                if (!array_key_exists("site_name", $option)) {
                    $option["site_name"] = frame::configGet("site/name");
                }

                if (!array_key_exists("site_url", $option)) {
                    $option["site_url"] = frame::configGet("site/url");
                }

                if (!array_key_exists("system_version", $option)) {
                    $option["system_version"] = frame::configGet("version/system");
                }

                if (!array_key_exists("debug", $option)) {
                    $option["debug"] = frame::configGet("system/debug");
                }
            }

            if (defined("FRAMEWORK_MODULELOAD/base")) {
                if (!array_key_exists("request_id", $option)) {
                    $option["request_id"] = frame::getRequestID();
                }
            }

            if (defined("FRAMEWORK_EXCEPTION_FORMAT_JSON")) {
                $format_json = TRUE;
            }

            require_once dirname(__FILE__) . "/exception-template.php";
            exit;
        }

        public static function framework_exception_upgrade_browser($home = "/") {
            require_once dirname(__FILE__) . "/upgrade-browser.php";
            exit;
        }
    }

    function framework_throwable_handle(Throwable $e) {
        $trace = array_merge([
            [
                "file" => $e->getFile(),
                "line" => $e->getLine()
            ]
        ], $e->getTrace());
        
        if (defined("FRAMEWORK_MODULELOAD/log")) {
            frame::log(-1, -1, $e->getMessage(), "", $trace);
        } else {
            FrameworkException::framework_exception_print(-1, $e->getMessage(), $trace);
        }
    }

    if (!defined("FRAMEWORK_EXCEPTION_HANDLE_DISABLED")) {
        set_exception_handler('framework_throwable_handle');
    }
?>
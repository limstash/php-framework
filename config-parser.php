<?php
    /**
     * Framework Module: config-parser
     * Parser config file
     * Link : https://blog.csdn.net/myweishanli/article/details/45098693
     * 
     * @package framework
     * @since 5.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class FrameworkConfig {
        static $config = [];

        public static function init($config) {
            self::$config = $config;
        }

        public static function set($key, $val) {
            $data = &self::$config;
            $path = explode("/", $key, 50);

            foreach ($path as $name) {
                if (!array_key_exists($name, $data)) {
                    $data[$name] = [];
                }

                $data = &$data[$name];
            }

            $data = $val;
        }

        public static function get($key) {
            $data = self::$config;
            $path = explode("/", $key, 50);

            foreach ($path as $name) {
                if (!array_key_exists($name, $data)) {
                    return null;
                }

                $data = $data[$name];
            }

            return $data;
        }
    }

    frame::__extend("configGet", function($key) {
        return FrameworkConfig::get($key);
    });

    frame::__extend("configSet", function($key, $val) {
        return FrameworkConfig::set($key, $val);
    });

    frame::__extend("configParser", function($config) {
        FrameworkConfig::init($config);
    });
?>
<?php

	/**
	 * Framework: Basic operations
	 *
	 * @package framework
	 * @since 1.0
	 */

    if(!defined("load")){
        header("Location:/404");
    }

    class Frame {
		static $extend_funcs = [];

		public static function  __callStatic($func, $args) {
			if (isset(self::$extend_funcs[$func])) {
				$extendFunc=self::$extend_funcs[$func];
				return call_user_func_array($extendFunc, $args);
			}

			throw new Exception("Framework function {$func} not exists");
		}

		public static function __extend($method, $func) {
			if (!is_callable($func)) {
				throw new Exception("Extend framework function {$method} is not callable");
			}

			self::$extend_funcs[$method] = $func;
		}

		/**
		 * Get const value
		 * 
		 * @param string $name
         * @return mixed
		 */

		public static function getConst($name) {
			if (!defined($name)) {
				return null;
		 	}
			return constant($name);
		}
        /**
         * Check if the assertion is FALSE
         * 
         * @param bool $result
         * The statement
         * 
         * @param string $text
         * The purpose or description of the assertion
         */

        public static function assert($result, $text = "No details") {
            if ($result == FALSE || $result == 0) {
				throw new Exception($text);
            }
		}

        /**
         * Get Request ID
         * 
         * Generate first if it does not exists, consist of timestamp and 10-digits rand number
		 * 
		 * @return string
         */

        public static function getRequestID() {
            if (!defined("RequestID")) {
                define("RequestID", dechex(floor(microtime(true)*1000)).self::randString(10));
            }
            return self::getConst("RequestID");
        }

        /**
		 * Generate Random String
		 * 
		 * @param int $length
		 * the length of the string
		 * @param string $char
		 * charset
		 * @return string
		 */

		public static function randString($length = 128, $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
			self::assert(is_int($length), "length must be integer");
			self::assert($length > 0, "length must be larger than 0");
		
			$string = '';
			for($i = $length; $i > 0; $i--) {
				$string .= $char[random_int(0, strlen($char) - 1)];
			}
		
			return $string;
        }
        
        /**
		 * One-way Encryption
		 *
		 * @param string $text
		 * Plaintext
		 * @param string $key
		 * Encryption key
         * @param int $times
         * Number of iterations
		 * @param int $length
		 * Length of Ciphertext
		 * @return string
		 */

		public static function oneWayEncryption($text, $key, $length = 256){
			self::assert(is_int($length), "length must be integer");
			self::assert($length <= 256, "length must be less or equal to 256");
			self::assert($length > 0, "length must be larger than 0");
			
			$part1 = hash("sha3-512", $key.$text);
			$part2 = hash("sha3-512", $text.$key);
			
			return substr($part1, 0, ceil($length/2)).substr($part2, -$length/2);
        }

        /**
        * Two-way Encrpytion

        * @param string $text
        * Plaintext
        * @param string $key
        * Encryption key
        * @param string $salt
        * Encrpytion salt
        * @return string
        */

		public static function twoWayEncryption($text, $key, $salt = ""){	
            $exchangeKey = self::oneWayEncryption($salt, $key, 32);

            $ivlen = openssl_cipher_iv_length("AES-256-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);

			$ciphertext = openssl_encrypt($text, "AES-256-CBC", $exchangeKey, OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha3-512', $ciphertext , $exchangeKey, true);

            return base64_encode($iv.$hmac.$ciphertext);   
		}

		/**
		 * Two-way Decrpytion
		 * 
		 * While fail to decrypt return FALSE instead
		 * 
		 * @param string $text
		 * Ciphertext
		 * @param string $key
		 * Encryption key 
		 * @param string $salt
		 * Encrpytion salt
		 * @return string
		 */

		public static function twoWayDecryption($text, $key, $salt = ""){
            $exchangeKey = self::oneWayEncryption($salt, $key, 32);

            $cipher = base64_decode($text);
            $ivlen = openssl_cipher_iv_length("AES-256-CBC");
            $iv = substr($cipher, 0, $ivlen);

            $hmac = substr($cipher, $ivlen, $sha3len=64);
            $ciphertext = substr($cipher, $ivlen+$sha3len);
			$plaintext = openssl_decrypt($ciphertext, "AES-256-CBC", $exchangeKey, OPENSSL_RAW_DATA, $iv);
			
            $calcmac = hash_hmac('sha3-512', $ciphertext, $exchangeKey, true);

            if (hash_equals($hmac, $calcmac)) {
                return $plaintext;
            } else {
                return false;
            }
        }

		/**
		 * Get Visitor IP
		 * 
		 * @return string
		 */

		public static function getIP() {
            if (defined("FRAMEWORK_OVERRIDE_IP")) {
                return constant("FRAMEWORK_OVERRIDE_IP");
            } else if (php_sapi_name() == "cli") {
				return "cli";
			} else if (@$_SERVER["HTTP_X_FORWARDED_FOR"]) {
				return $_SERVER["HTTP_X_FORWARDED_FOR"];
			} else if (@$_SERVER["HTTP_CLIENT_IP"]) {
				return $_SERVER["HTTP_CLIENT_IP"];
			} else if (@$_SERVER["REMOTE_ADDR"]) {
				return $_SERVER["REMOTE_ADDR"];
			} else if (@getenv("HTTP_X_FORWARDED_FOR")) {
				return getenv("HTTP_X_FORWARDED_FOR");
			} else if (@getenv("HTTP_CLIENT_IP")) {
				return getenv("HTTP_CLIENT_IP");
			} else if (@getenv("REMOTE_ADDR")) {
				return getenv("REMOTE_ADDR");
			} else {
				return "Unknown";
			}            
		}

		/**
		 * Ger Request URL
		 * 
		 * @return string
		 */
		
		public static function getURL(){
			if (php_sapi_name() == "cli") {
				return "cli";
			} else if($_SERVER["SERVER_PORT"] == 443) {
				return 'https://'.$_SERVER['SERVER_NAME'].':'.$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"]; 
			} else {
				return 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"]; 
			}
		}
        
        /**
		 * Get User Agent
		 * 
		 * @return string
		 */

        public static function getUserAgent() {
            return $_SERVER['HTTP_USER_AGENT'];
        }

		/**
		 * Get Visitor Browser Info
		 * 
		 * @return string
		 */

		public static function getBrowser(){
			if (php_sapi_name() == "cli") {
				return "cli";
            }
            
            $browser = self::getUserAgent();

            if (empty($browser)) {
                return "Unknown";
            } else {
                return $browser;
            }
		} 

		/**
		 * Get Visitor Operator System
		 * 
		 * @param null
		 * @return string
		 */

		public static function getOS(){
			if (php_sapi_name() == "cli") {
				return "cli";
			} else if (!empty(self::getUserAgent())) {
				$os = self::getUserAgent();

				if (preg_match('/android/i', $os)) {
					$os = "Android";
				} else if (preg_match('/iphone/i', $os) || preg_match('/ipad/i', $os)) {
					$os = "iOS";
				} else if (preg_match('/win/i', $os)) {
					$os = 'Windows';
				} else if (preg_match('/mac/i', $os)) {
					$os = 'MAC';
				} else if (preg_match('/linux/i', $os)) {
					$os = 'Linux';
				} else if (preg_match('/unix/i', $os)) {
					$os = 'Unix';
				} else if (preg_match('/bsd/i', $os)) {
					$os = 'BSD';
				} else {
					$os = 'Other';
				}
				return $os;
			} else {
				return 'Unknown';
			}
		}

		/**
		 * Get Absolute URL
		 * 
		 * @return string
		 */

		public static function getAbsoluteURL(){
			$url = self::getURL();
			return parse_url($url)["path"];
		}

        /**
         * Style filepath and remove sensitive info
         * 
         * @param string $path
         * @return string
         */
		
        public static function styleFilepath($path) {
            $path = str_replace('\\', '/', $path);
            $path = str_replace(constant("ABSPATH"), "", $path);
            return htmlspecialchars($path);
        }

		/**
		 * Init session
		 */

		public static function initSession() {
            if (session_status() === PHP_SESSION_ACTIVE) {
                return;
            } 

			$cookie_params = self::getCookieOption();
			$cookie_params["lifetime"] = 30 * 24 * 3600;
			unset($cookie_params["expires"]);
			
			session_set_cookie_params($cookie_params);
			session_start();

			frame::clientKey();
		}

		/**
		 * Create a session
		 * 
		 * @param string $name
		 * @param string $text
		 */

		public static function createSession($name, $text){
			$_SESSION[$name] = $text;
		}

		/**
		 * Check if session exists
		 * 
		 * @param string $name
		 * @return bool
		 */
		
		public static function issetSession($name){
			if(!empty($_SESSION[$name])){
				return true;
			}

			return false;
		}

		/**
		 * Read a session
		 * 
		 * @param string $name
		 */
		
		public static function readSession($name){
			if (!self::issetSession($name)) {
				return null;
			}
			return $_SESSION[$name];
		}
		
		/**
		 * Delete a Session
		 * 
		 * @param string $name
		 */

		public static function deleteSession($name){
			unset($_SESSION[$name]);
		}

		/**
		 * Get Cookie Option
		 * 
		 * @param int $time
		 * @param string $path
		 * @param bool $secure
		 * @param bool $httponly
		 * 
		 * @return array
		 */

		private static function getCookieOption($time = 0, $path = "/", $secure = true, $httponly = true, $samesite = "Lax") {
			return [
				'expires' => ($time == 0) ? 0 : time() + $time,
				'path' => $path,
				'secure' => $secure,
				'httponly' => $httponly,
				'samesite' => $samesite
			];
		}

		/**
		 * Create cookie
		 * 
		 * When the cookieTime is set to 0，it will be deleted when the browser close.
		 * 
		 * @param string $name
		 * @param string $text
		 * @param int $time
		 * 
		 * @return null
		 */

		public static function createCookie($name, $text, $time = 7 * 24 * 3600){
			$cookie_options = self::getCookieOption($time);
			
			setcookie($name, self::twoWayEncryption($text, self::configGet("/security/master"), self::configGet("/security/cookie").self::clientKey()), $cookie_options);
			$_COOKIE[$name] = $text;
		}

		/**
		 * Check if cookie exists
		 * 
		 * @param string $name
		 * @return bool
		 */

		public static function issetCookie($name){
			if(!empty($_COOKIE[$name])){
				if(self::twoWayDecryption($_COOKIE[$name], self::configGet("/security/master"), self::configGet("/security/cookie").self::clientKey()) != FALSE)
					return TRUE;
			}

			return FALSE;
		}

		/**
		 * Read a cookie
		 * 
		 * @param string $name
		 * @return string | null
		 */

		public static function readCookie($name){
			if(!empty($_COOKIE[$name])){
				return self::twoWayDecryption($_COOKIE[$name], self::configGet("/security/master"), self::configGet("/security/cookie").self::clientKey());
			}

			return null;			
		}

		/**
		 * Delete a cookie
		 * 
		 * @param string $name
		 * @param int $time
		 */

		public static function deleteCookie($name){
			$cookie_options = self::getCookieOption(time() - 3600);
			setcookie($name, "", $cookie_options);
		}

		/**
		 * Generate Client Key
		 * 
		 * @return string
		 */

		public static function clientKey(){
			if (!isset($_SESSION['_key'])){
				$key = self::randString(32, "abcdefghijklmnopqrstuvwxyz0123456789");
				$_SESSION["_key"] = $key;
			} else {
				$key = $_SESSION['_key'];
			}
			return $key;
		}

		/**
		 * Show error messages
		 * 
		 * @param string $code
		 * Error code
		 * @param string $name
		 * Error name
		 * @param string $siteName
		 * Site name
		 * @param string $siteUrl
		 * Site link
		 * @param array $stack
		 * Error stack
		 */

		public static function printErrorMessage($code, $name, $options = [], $stack = NULL){
			$FrameworkException = new FrameworkException();
			$FrameworkException->framework_exception_print($code, $name, $stack, $options);
		}

		/**
		 * Return HTTP Error Code
		 * 
		 * @param int $code
		 */

		public static function HTTPCode($code){
			static $http = array (
				100 => "HTTP/1.1 100 Continue",
				101 => "HTTP/1.1 101 Switching Protocols",
				200 => "HTTP/1.1 200 OK",
				201 => "HTTP/1.1 201 Created",
				202 => "HTTP/1.1 202 Accepted",
				203 => "HTTP/1.1 203 Non-Authoritative Information",
				204 => "HTTP/1.1 204 No Content",
				205 => "HTTP/1.1 205 Reset Content",
				206 => "HTTP/1.1 206 Partial Content",
				300 => "HTTP/1.1 300 Multiple Choices",
				301 => "HTTP/1.1 301 Moved Permanently",
				302 => "HTTP/1.1 302 Found",
				303 => "HTTP/1.1 303 See Other",
				304 => "HTTP/1.1 304 Not Modified",
				305 => "HTTP/1.1 305 Use Proxy",
				307 => "HTTP/1.1 307 Temporary Redirect",
				400 => "HTTP/1.1 400 Bad Request",
				401 => "HTTP/1.1 401 Unauthorized",
				402 => "HTTP/1.1 402 Payment Required",
				403 => "HTTP/1.1 403 Forbidden",
				404 => "HTTP/1.1 404 Not Found",
				405 => "HTTP/1.1 405 Method Not Allowed",
				406 => "HTTP/1.1 406 Not Acceptable",
				407 => "HTTP/1.1 407 Proxy Authentication Required",
				408 => "HTTP/1.1 408 Request Time-out",
				409 => "HTTP/1.1 409 Conflict",
				410 => "HTTP/1.1 410 Gone",
				411 => "HTTP/1.1 411 Length Required",
				412 => "HTTP/1.1 412 Precondition Failed",
				413 => "HTTP/1.1 413 Request Entity Too Large",
				414 => "HTTP/1.1 414 Request-URI Too Large",
				415 => "HTTP/1.1 415 Unsupported Media Type",
				416 => "HTTP/1.1 416 Requested range not satisfiable",
				417 => "HTTP/1.1 417 Expectation Failed",
				500 => "HTTP/1.1 500 Internal Server Error",
				501 => "HTTP/1.1 501 Not Implemented",
				502 => "HTTP/1.1 502 Bad Gateway",
				503 => "HTTP/1.1 503 Service Unavailable",
				504 => "HTTP/1.1 504 Gateway Time-out"
			);
			
			header($http[$code]);
		}

		/**
		 * Redirect to error pages
		 * 
		 * @param int $code
		 */
		public static function redirectErrorPages($code) {
			header("Location:/".$code);
			exit;
		}

		/**
		 * Show upgrade browser page
		 * 
		 * @param null
		 */

		public static function upgradeBrowser() {
			FrameworkException::framework_exception_upgrade_browser();
		}
	}
?>
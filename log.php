<?php
    /**
     * Framework Module: log
     * Basic File Log
     * 
     * @package framework
     * @since 5.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class FrameworkLog {
       /**
         * Write log into file
         * 
         * @param int $logLevel
         * log level
         * @param int $logCode
         * log code
         * @param string $logTitle
         * log text
         * @param string $logInfo
         * log info
         * @param string $execStack
         * log stack
         * 
         */

        public static function basicLog($logLevel, $logCode, $logTitle, $logInfo, $execStack){
            $logFilePath = frame::getConst("ABSPATH")."data/log/".date("Ymd").".log";
            $logFileStream = fopen($logFilePath, "a+");

            if ($logFileStream == false) {
                return;
            }

            $date = date("Y-m-d H:i:s");
            $logURL = frame::getURL();
            $guestIP = frame::getIP();
            $guestOS = frame::getOS();
            $guestBrowser = frame::getBrowser();
            $requestID = frame::getRequestID();

            $fieldGet = json_encode($_GET);
            $fieldPost = json_encode($_POST);
            $fieldCookie = json_encode($_COOKIE);
            $fieldSession = json_encode($_SESSION);

            if (defined("FRAMEWORK_MODULELOAD/config-parser")) {
                $logHash = frame::oneWayEncryption(date("Y-m-d H:i").$execStack.$fieldGet.$fieldPost.$fieldCookie.$fieldSession.$guestIP.$guestOS.$guestBrowser.$logURL.$logInfo, frame::configGet("secuirty/log"), 64);
            } else {
                $logHash = frame::oneWayEncryption(date("Y-m-d H:i").$execStack.$fieldGet.$fieldPost.$fieldCookie.$fieldSession.$guestIP.$guestOS.$guestBrowser.$logURL.$logInfo, null, 64);                
            }

            if ($logLevel == -1) {
                $logLevel = "Fatal";
            } else if ($logLevel == 1) {
                $logLevel = "Error";
            } else if ($logLevel == 2) {
                $logLevel = "Warning";
            } else if ($logLevel == 3) {
                $logLevel = "Notice";
            } else {
                $logLevel = "Debug";
            }

            $log = sprintf("%s | %s | %s | %s | %s | %s | %s | %s | %s \n", $date, $logLevel, $logCode, $logTitle, $logURL, $guestIP, $guestBrowser, $requestID, $logHash);
            
            if (!flock($logFileStream, LOCK_EX)){
                return;
            }

            fwrite($logFileStream, $log);

            if ($logLevel == "Fatal") {
                $trace = sprintf("Trace: %s \n", $execStack);
                fwrite($logFileStream, $trace);
            }

            flock($logFileStream, LOCK_UN);
            fclose($logFileStream);
        }
    }

    frame::__extend("basicLog", function($level, $code, $title, $info = "", $execStack = null) {
        if ($level == 4 && defined("FRAMEWORK_MODULELOAD/config-parser") && frame::configGet("system/debug") != TRUE) {
            return;
        }
        
        if ($execStack == null) {
            $execStack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        }

        FrameworkLog::basicLog($level, $code, $title, $info, json_encode($execStack));
    });

    frame::__extend("log", function($level, $code, $title, $info = "", $execStack = null) {
        if ($level == 4 && defined("FRAMEWORK_MODULELOAD/config-parser") && frame::configGet("system/debug") != TRUE) {
            return;
        }
        
        if ($execStack == null) {
            $execStack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        }

        FrameworkLog::basicLog($level, $code, $title, $info, json_encode($execStack));

        if ($level == -1 || $level == 1) {
            FrameworkException::framework_exception_print($code, $title, $execStack);
        }
    });
?>
<?php

	/**
	 * Framework: Load
	 *
	 * @package framework
	 * @since 5.0
	 */


    if (!defined("load")) {
        header("Location:/404");
        exit;
    }

    define("framework_version", "6.2");
    define("framework_ipdatabase_v4_version", "20220420");
    define("framework_ipdatabase_v6_version", "20210726");

    $modules = [
        "config-parser",
        "ip-database",
        "log",
        "system-monitor",
        "model/db",
        "model/cache",
        "model/route"
    ];

    function framework_load_module($path) {
        if (!include_once dirname(__FILE__) . '/' . $path . ".php") {
            throw New Exception("FrameworkException: Failed to load module {$path}");
        };
        define("FRAMEWORK_MODULELOAD/{$path}", true);
    }

    framework_load_module("exception/exception");
    framework_load_module("base");

    foreach ($modules as $module) {
        $option_name = strtoupper(str_replace("/", "_", $module));
        $option_name = "FRAMEWORK_MODEL_" . $option_name . "_DISABLED";

        if (!defined($option_name)) {
            framework_load_module($module);
        }
    }
?>
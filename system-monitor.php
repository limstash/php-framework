<?php

    /**
     * Get System Info
     * Link : https://blog.csdn.net/myweishanli/article/details/45098693
     * 
     * @package framework
     * @since 4.0
     */


    if (!defined("load")) {
        header("Location:/404");
        exit;
    }

    class FrameworkSystemMonitor {
        
        /**
         * 服务器运行时间
         *
         * @return string
         */

        public function GetUpTime() {
            if (false === ($str = file_get_contents("/proc/uptime")))
                return '';

            $upTime = '';
            $str    = explode(" ", $str);
            $str    = trim($str[0]);
            $min    = $str / 60;
            $hours  = $min / 60;
            $days   = (int)($hours / 24);
            $hours  = $hours % 24;
            $min    = $min % 60;

            if ($days !== 0) {
                $upTime = $days . "d ";
            }
            if ($hours !== 0) {
                $upTime .= $hours . "h ";
            }

            return $upTime . $min . "m ";
        }

        /**
         * 内存信息
         *
         * @param bool $bFormat 格式化
         *
         * @return array
         */

        public function GetMem(bool $bFormat = false) {
            if (false === ($str = file_get_contents("/proc/meminfo")))
                return [];

            preg_match_all("/MemTotal\s{0,}\:+\s{0,}([\d\.]+).+?MemFree\s{0,}\:+\s{0,}([\d\.]+).+?Cached\s{0,}\:+\s{0,}([\d\.]+).+?SwapTotal\s{0,}\:+\s{0,}([\d\.]+).+?SwapFree\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $mems);
            preg_match_all("/Buffers\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $buffers);

            $mtotal    = $mems[1][0] * 1024;
            $mfree     = $mems[2][0] * 1024;
            $mbuffers  = $buffers[1][0] * 1024;
            $mcached   = $mems[3][0] * 1024;
            $stotal    = $mems[4][0] * 1024;
            $sfree     = $mems[5][0] * 1024;
            $mused     = $mtotal - $mfree;
            $sused     = $stotal - $sfree;
            $mrealused = $mtotal - $mfree - $mcached - $mbuffers; //真实内存使用

            $rtn['mTotal']         = !$bFormat ? $mtotal : $this->size_format($mtotal, 1);
            $rtn['mFree']          = !$bFormat ? $mfree : $this->size_format($mfree, 1);
            $rtn['mBuffers']       = !$bFormat ? $mbuffers : $this->size_format($mbuffers, 1);
            $rtn['mCached']        = !$bFormat ? $mcached : $this->size_format($mcached, 1);
            $rtn['mUsed']          = !$bFormat ? ($mtotal - $mfree) : $this->size_format($mtotal - $mfree, 1);
            $rtn['mPercent']       = (floatval($mtotal) != 0) ? round($mused / $mtotal * 100, 1) : 0;
            $rtn['mRealUsed']      = !$bFormat ? $mrealused : $this->size_format($mrealused, 1);
            $rtn['mRealFree']      = !$bFormat ? ($mtotal - $mrealused) : $this->size_format($mtotal - $mrealused, 1); //真实空闲
            $rtn['mRealPercent']   = (floatval($mtotal) != 0) ? round($mrealused / $mtotal * 100, 1) : 0;             //真实内存使用率
            $rtn['mCachedPercent'] = (floatval($mcached) != 0) ? round($mcached / $mtotal * 100, 1) : 0;              //Cached内存使用率
            $rtn['swapTotal']      = !$bFormat ? $stotal : $this->size_format($stotal, 1);
            $rtn['swapFree']       = !$bFormat ? $sfree : $this->size_format($sfree, 1);
            $rtn['swapUsed']       = !$bFormat ? $sused : $this->size_format($sused, 1);
            $rtn['swapPercent']    = (floatval($stotal) != 0) ? round($sused / $stotal * 100, 1) : 0;

            return $rtn;
        }

        /**
         * 获取CPU使用率
         *
         * @return bool|array
         */

        public function GetCPU() {
            if (!function_exists("popen")) {
                return false;
            }
            
            $fp = popen('top -b -n 1 | grep -E "(Cpu\(s\))"', "r");
            $rs = '';
            while(!feof($fp)){
                $rs .= fread($fp, 1024);  
            }
            pclose($fp);
            
            $sys_info = explode("\n", $rs);
            return explode("%Cpu(s):", $sys_info[0])[1];
        }

        /**
         * 获取系统负载
         *
         * @return array|false|string[]
         */

        public function GetLoad() {
            if (false === ($str = file_get_contents("/proc/loadavg")))
                return [];

            $loads = explode(' ', $str);
            if ($loads) {
                return [
                    '1m'  => $loads[0],
                    '5m'  => $loads[1],
                    '15m' => $loads[2],
                ];
            }

            return [];
        }

        /**
         * 获取网络数据
         *
         * @param bool $bFormat
         *
         * @return array
         */

        public function GetNetwork(bool $bFormat = false) {
            $rtn     = [];
            $netstat = file_get_contents('/proc/net/dev');
            if (false === $netstat) {
                return [];
            }

            $bufe = preg_split("/\n/", $netstat, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($bufe as $buf) {
                if (preg_match('/:/', $buf)) {
                    list($dev_name, $stats_list) = preg_split('/:/', $buf, 2);
                    $dev_name = trim($dev_name);

                    $stats                        = preg_split('/\s+/', trim($stats_list));
                    $rtn[$dev_name]['name']       = $dev_name;
                    $rtn[$dev_name]['in_rate']    = !$bFormat ? $stats[0] : $this->netSize($stats[0]);
                    $rtn[$dev_name]['in_packets'] = $stats[1];
                    $rtn[$dev_name]['in_errors']  = $stats[2];
                    $rtn[$dev_name]['in_drop']    = $stats[3];

                    $rtn[$dev_name]['out_traffic'] = !$bFormat ? $stats[8] : $this->netSize($stats[8]);
                    $rtn[$dev_name]['out_packets'] = $stats[9];
                    $rtn[$dev_name]['out_errors']  = $stats[10];
                    $rtn[$dev_name]['out_drop']    = $stats[11];
                }
            }

            return $rtn;
        }

        public function size_format($bytes, $decimals = 2) {
            $quant = array(
                'TB' => 1099511627776, // pow( 1024, 4)
                'GB' => 1073741824, // pow( 1024, 3)
                'MB' => 1048576, // pow( 1024, 2)
                'KB' => 1024, // pow( 1024, 1)
                'B ' => 1,
            );

            foreach ($quant as $unit => $mag) {
                if (doubleval($bytes) >= $mag) {
                    return number_format($bytes / $mag, $decimals) . ' ' . $unit;
                }
            }

            return "0 B";
        }

        public function netSize($size, $decimals = 2) {
            if ($size < 1024) {
                $unit = "Bbps";
            } else if ($size < 10240) {
                $size = round($size / 1024, $decimals);
                $unit = "Kbps";
            } else if ($size < 102400) {
                $size = round($size / 1024, $decimals);
                $unit = "Kbps";
            } else if ($size < 1048576) {
                $size = round($size / 1024, $decimals);
                $unit = "Kbps";
            } else if ($size < 10485760) {
                $size = round($size / 1048576, $decimals);
                $unit = "Mbps";
            } else if ($size < 104857600) {
                $size = round($size / 1048576, $decimals);
                $unit = "Mbps";
            } else if ($size < 1073741824) {
                $size = round($size / 1048576, $decimals);
                $unit = "Mbps";
            } else {
                $size = round($size / 1073741824, $decimals);
                $unit = "Gbps";
            }

            $size .= $unit;

            return $size;
        }
    }

    /**
     * Get System Info
     * 
     * @return array
     */

    frame::__extend("getSystemInfo", function () {
        $monitor = new FrameworkSystemMonitor();
        $mem = $monitor->GetMem();

        return array(
            "uptime" => $monitor->GetUpTime(),
            "load" => $monitor->GetLoad(),
            "cpu" => $monitor->GetCPU(),
            "mem" => array(
                "total" => $monitor->size_format($mem["mTotal"]),
                "free" => $monitor->size_format($mem["mFree"]),
                "buffers" => $monitor->size_format($mem["mBuffers"]),
                "cached" => $monitor->size_format($mem["mCached"]),
                "used" => $monitor->size_format($mem["mUsed"]),
                "realused" => $monitor->size_format($mem["mRealUsed"]),
                "percent" => $mem["mPercent"],
            ),
            "swap" => array(
                "total" => $monitor->size_format($mem["swapTotal"]),
                "used" => $monitor->size_format($mem["swapUsed"]),
                "percent" => $mem["swapPercent"]
            )
        );
    });

    frame::__extend("sizeFormat", function ($value) {
        $monitor = new FrameworkSystemMonitor();
        return $monitor->size_format($value);        
    });
?>
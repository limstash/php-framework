## A Lightweight PHP Base Framework

- Base/Browser: Cookie/Session operation, CSRF Token, HTTP Status Code, client information (browser & IP adddress & OS & request URL)
- Base/Crypto: SHA3-512 encrpytion, AES-256-CBC encryption & decryption
- Exception/upgrade-browser: upgrade tips for IE
- Exception/exception: handle exception
- Library/ConfigParser: parse config array to path tree
- Library/IPDatabase: get IPv4 & IPv6 geographical location 
- Library/Log: basic file log operation
- Library/SystemMonitor: uptime, load, cpu, memory, swap, network usage
- Model/Cache: Redis operation
- Model/DB: MySQL operation
- Model/Route: Pseudo-static operation


### LICENSE
```
Copyright 2020-2022 Limstash Wong (limstash.w@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
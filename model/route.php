<?php

    /**
     * HTML operation
     * 
     * @since 1.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class route{ 
        private static $header = [];
        private static $footer = [];
        private static $sources = [];

        private static $title = "首页";
        private static $assets = [];

        private static $assetsFlag = true;
        private static $routeMatch = false;
        private static $exclusive = false;

        private static $route_data;

        /**
         * Set data
         * 
         * @param array $data
         * @return null
         */

        public static function setData($data) {
            self::$route_data = $data;
        }

        public static function header($src) {
            self::append($src, 0);
        }

        public static function footer($src) {
            self::append($src, 2);
        }

        public static function title($title) {
            self::$title = $title;
        }
        
        /**
         * Append source file
         * 
         * @param string $src
         * @param int $flag
         */

        public static function append($src, $flag){
            if (self::$exclusive) {
                return null;
            }
            
            if ($flag == 0) {
                array_push(self::$header, $src);
            } else if ($flag == 1) {
                self::$routeMatch = true;
                array_push(self::$sources, $src);
            } else {
                array_push(self::$footer, $src);
            }

            $assetsFlag = self::$assetsFlag;
            $assets = &self::$assets;
            $route_data = self::$route_data;

            if(!include(frame::getConst("ABSPATH")."/lib/{$src}.php")){
                frame::log(1, 111, "Route Append Failed ({$src}) - No such file");
            }
        }

        /**
         * Route request
         * 
         * @param string $path
         * router url path
         * @param string $file
         * router file path
         * @param string $title
         * site page name
         * @param int $type
         * Non-Exclusive (0) / Exclusive (1)
         * 
         * @return bool
         */

        public static function route($path, $file, $title, $type){
            $routePath = str_replace("/","\\/",$path);
            $rstr = frame::getAbsoluteURL();

            if(preg_match("/^".$routePath."$/", $rstr)){
                self::$title = $title;
                
                if ($type == 1) {
                    self::append($file, 1);
                    self::$exclusive = true;
                } else {
                    self::append($file, 1);
                }

                return true;
            }

            return false;
        }

        /**
         * Print HTML
         * 
         * @param NULL
         */

        public static function print(){
            if (self::$exclusive) return;
            
            $assetsFlag = self::$assetsFlag = false;
            $assets = &self::$assets;
            $route_data = self::$route_data;
            $title = self::$title;

            foreach(self::$header as $name){
                if(!include(frame::getConst("ABSPATH")."/lib/{$name}.php")){
                    frame::log(1, 112, "Route Print Failed ({$name}) - Include Failed");
                }
            }

            foreach(self::$sources as $name){
                if(!include(frame::getConst("ABSPATH")."/lib/{$name}.php")){
                    frame::log(1, 112, "Route Print Failed ({$name}) - Include Failed");
                }
            }

            foreach(self::$footer as $name){
                if(!include(frame::getConst("ABSPATH")."/lib/{$name}.php")){
                    frame::log(1, 112, "Route Print Failed ({$name}) - Include Failed");
                }
            }
        }

        /**
         * Route function exclusively
         * 
         * @param string $path
         * @param callable $function
         * @param array $functionArgs
         */

        public static function routeFunctionExclusive($path, $function, $functionArgs = []) {
            if (!is_callable($function)) {
                return null;
            }

            $routePath = str_replace("/","\\/",$path);
            $rstr = frame::getAbsoluteURL();

            if(preg_match("/^".$routePath."$/", $rstr)){
                self::$routeMatch = true;
                self::$exclusive = true;
                call_user_func_array($function, $functionArgs);
            }
        }

        public static function include($src) {
            $route_data = self::$route_data;
            $assetsFlag = self::$assetsFlag;
            $assets = &self::$assets;
            $title = self::$title;
            
            if(!include(frame::getConst("ABSPATH")."/lib/{$src}.php")){
                frame::log(1, 111, "Route Include Failed ({$src}) - Include Failed");
            }
        }

        /**
         * Check route status
         * 
         * @param NULL
         * @return NULL
         */
        
        public static function checkRoute(){
            if(!self::$routeMatch){
                frame::redirectErrorPages(404);
            }
        }
    }
?>
<?php

    /**
     * Cache Operation
     * 
     * @since 1.0
     */

    class Cache{
        public static function init() {
            global $redis;

            if (!class_exists("Redis")) {
                throw New Exception("FrameworkException: PHP Missing Redis Support");
            }

            $redis = new Redis();
            $redis->connect(frame::configGet("redis/ip"), frame::configGet("redis/port"));

            if (frame::configGet("redis/password")) {
                $redis->auth(frame::configGet("redis/password"));
            }

            $redis->select(frame::configGet("redis/db"));
            
            return $redis;
        }

        public static function version() {
            global $redis;
            return $redis->info()["redis_version"];
        }

        public static function getAddress() {
            $address = "tcp://" . frame::configGet("redis/ip") . ":" . frame::configGet("redis/port");

            if (frame::configGet("redis/password")) {
                $address .= "?auth=".frame::configGet("redis/password");
            }

            return $address;
        }

        private static function set($name, $value, $expire = 7 * 24 * 3600) {
            global $redis;
            $redis->set($name, $value);
            $redis->expire($name, $expire);
        }

        public static function exists($name) {
            global $redis;
            return $redis->exists($name);
        }

        private static function get($name) {
            global $redis;
            return $redis->get($name);
        }

        public static function unset($name) {
            global $redis;
            $redis->del($name);
        }

        public static function unsetPattern($pattern) {
            global $redis;
            $redis->del($redis->keys($pattern));
        }

        public static function setString($name, $func, $functionArgs, $expire = 7 * 24 * 3600) {
            $res = call_user_func_array($func, $functionArgs);

            if (!is_null($res)) {
                self::set($name, $res, $expire);
            }

            return $res;
        }

        public static function setArraySingle($name, $func, $functionArgs, $expire = 7 * 24 * 3600) {
            $res = call_user_func_array($func, $functionArgs);

            if (!is_null($res)) {
                self::set($name, json_encode($res), $expire);
            }

            return $res;
        }

        public static function setArrayMulti($name, $hash, $data, $func, $functionArgs, $expire = 7 * 24 * 3600) {
            $res = call_user_func_array($func, $functionArgs);

            if (!is_null($res)) {
                $data[$hash] = $res;
                self::set($name, json_encode($data), $expire);
            }

            return $data;
        }

        private static function getArraySingle($name, $func, $functionArgs, $expire) {
            if (self::exists($name)) {
                return json_decode(self::get($name), true);
            }

            return self::setArraySingle($name, $func, $functionArgs, $expire);
        }

        private static function getArrayMulti($name, $hash, $func, $functionArgs, $expire) {
            if (self::exists($name)) {
                $data = json_decode(self::get($name), true);
            }

            if (isset($data[$hash])) {
                return $data[$hash];
            } else {
                return self::setArrayMulti($name, $hash, $data, $func, $functionArgs, $expire)[$hash];
            }
        }

        public static function getString($name, $func, $functionArgs, $expire = 7 * 24 * 3600) {
            if (self::exists($name)) {
                return self::get($name);
            }

            return self::setString($name, $func, $functionArgs, $expire);
        }

        public static function getArray($name, $func, $functionArgs = [], $hash = "", $expire = 7 * 24 * 3600) {
            if (empty($hash)) {
                return self::getArraySingle($name, $func, $functionArgs, $expire);
            } else {
                return self::getArrayMulti($name, $hash, $func, $functionArgs, $expire);
            }
        }

        public static function mapLen($bucket) {
            global $redis;
            return $redis->hLen($bucket);
        }
        
        public static function mapGet($bucket, $name) {
            global $redis;
            return $redis->hGet($bucket, $name);
        }

        public static function mapGetAll($bucket) {
            global $redis;
            return $redis->hGetAll($bucket);
        }

        public static function mapInsert($bucket, $name, $value, $expire = 7 * 24 * 3600) {
            global $redis;
            $redis->hSet($bucket, $name, $value);
            $redis->expire($bucket, $expire);
        }

        public static function mapDelete($bucket, $name) {
            global $redis;
            $redis->hDel($bucket, $name);
        }
    }
?>
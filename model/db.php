<?php

    /**
     * Database Operation
     * 
     * @since 1.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class DB{
        protected static $conn;
        protected static $data;

        public static function set($ip, $username, $password, $database, $port, $tableprefix) {
            static::$data = [
                "ip" => $ip,
                "username" => $username,
                "password" => $password,
                "database" => $database,
                "port" => $port,
                "tableprefix" => $tableprefix
            ];
        }

        private static function init() {
            try {
                static::$conn = new mysqli(static::$data["ip"], static::$data["username"], static::$data["password"], static::$data["database"], static::$data["port"]);
            } catch (Exception $e) {
                frame::log(1, 101, "Unable to connect to the database");
            }

            if (!static::$conn->set_charset("utf8mb4")) {
                frame::log(1, 101, "Unable to set charset");
            }

            return static::$conn;
        }

        public static function version() {
            if (!static::$conn) static::init();
            return mysqli_get_server_info(static::$conn);
        }

        public static function autocommit($enable) {
            mysqli_autocommit(static::$conn, $enable);
        }

        public static function rollback() {
            mysqli_rollback(static::$conn);
        }

        public static function commit() {
            mysqli_commit(static::$conn);
        }

        private static function prepare($q, $argt = "", $argv = []) {
            if (!static::$conn) static::init();
            $q = str_replace("TABLEPREFIX", static::$data["tableprefix"], $q);

            try {
                $stmt = static::$conn->prepare($q);
                if (!empty($argt)) {
                    $stmt->bind_param($argt, ...$argv);
                }
                $stmt->execute();
            } catch (Exception $e) {
                frame::log(2, 102, "SQL {{$q}} Prepared Failed : ".static::$conn->error);
            }

            return $stmt;
        }

        public static function insert($q, $argt = "", $argv = []) {
            $stmt = static::prepare($q, $argt, $argv);
            
            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(2, 103, "SQL Execute Failed : ".$stmt->error);
                return false;
            }

            if ($stmt -> affected_rows <= 0) {
                frame::log(4, 103, "No row affected ({$q}) {{$argt}} {".json_encode($argv)."}");
            }

            return $stmt->insert_id;
        }

        public static function update($q, $argt = "", $argv = []) {
            $stmt = static::prepare($q, $argt, $argv);

            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(2, 103, "SQL Execute Failed : ".$stmt->error);
                return false;
            }

            if ($stmt -> affected_rows <= 0) {
                frame::log(4, 103, "No row affected ({$q}) {{$argt}} {".json_encode($argv)."}");
            }

            return $stmt -> affected_rows;
        }

        public static function delete($q, $argt = "", $argv = []) {
            $stmt = static::prepare($q, $argt, $argv);

            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(2, 103, "SQL Execute Failed : ".$stmt->error);
                return false;
            }

            if ($stmt -> affected_rows <= 0) {
                frame::log(4, 103, "No row affected ({$q}) {{$argt}} {".json_encode($argv)."}");
            }

            return $stmt -> affected_rows;
        }

        public static function num_rows($q, $argt = "", $argv = []) {
            $stmt = static::prepare($q, $argt, $argv);

            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(1, 2, 103, "SQL Execute Failed : ".$stmt->error);
                return 0;
            }
            
            return $stmt->get_result()->num_rows;
        }

        public static function escape($q) {
            if (!static::$conn) static::init();
            return static::$conn->escape_string($q);
        }

        public static function selectFirst($q, $argt = "", $argv = [], $opt = MYSQLI_ASSOC) {
            $stmt = static::prepare($q, $argt, $argv);

            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(2, 103, "SQL Execute Failed : ".$stmt->error);
                return null;
            }

            return (array)$stmt->get_result()->fetch_array($opt);
        }

        public static function selectAll($q, $argt = "", $argv = [], $opt = MYSQLI_ASSOC) {
            $res = [];

            $stmt = static::prepare($q, $argt, $argv);
            
            if (!$stmt || $stmt -> errno != 0) {
                if ($stmt) frame::log(2, 103, "SQL Execute Failed : ".$stmt->error);
                return null;
            }

            $result = $stmt->get_result();
            
            while ($row = $result->fetch_array($opt)) {
                $res[] = $row;
            }
            
            return $res;
        }
    }
?>